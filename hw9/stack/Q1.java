//Stack: The Stack is always Last-In and First-Out

//--------------------------------------------------------------------------------------------------------------------------//
Public interface StackExercise:
//1-1. Creates an empty stack using the default or specified capacity.
top = 0;
stack = (T[])(new Object[defaultCapacity or specifiedCapacity])

//1-2. Adds the specified element to the top of the stack, expanding the capacity of the stack array if necessary.
if (size() == stack.length)

    expandCapacity();
    stack[top] = element;
    top++
//--------------------------------------------------------------------------------------------------------------------------//

Public String pop() throws StackEmptyException:
//2-1. Removes the element at the top of the stack and returns a reference to it.
//2-2. Throws an StackEmptyException if the stack is empty.
if (isEmpty())
    throw new EmptyStackException();

    top--;
    T result = stack[top];
    stack[top] = null;
    return result;
//--------------------------------------------------------------------------------------------------------------------------//

Public void push(String item):
//3-1. Returns a reference to the element at the top of the stack.
//3-2. The element is not removed from the stack. Throws an EmptyStackException if the stack is empty.
if (isEmpty())
    throw new EmptyStackException();

    return stack[top-1];

/boolean isEmpty():
//--------------------------------------------------------------------------------------------------------------------------//

//4-1. Returns true if the stack is empty and false otherwise.
public boolean isEmpty()
{
    return (top == 0);
}
//--------------------------------------------------------------------------------------------------------------------------//
