package stack3;

import jss2.exceptions.*;
import java.util.Iterator;

public class ArrayStack<T> implements StackADT<T>
{
   private final int DEFAULT_CAPACITY = 100;
   private int top;  // indicates the next open slot
   private transient T[] stack;

   public ArrayStack()
   {
      top = 0;
      stack = (T[])(new Object[DEFAULT_CAPACITY]);
   }

   public ArrayStack (int initialCapacity) // Creates an empty stack
   {
      top = 0;
      stack = (T[])(new Object[initialCapacity]);
   }

   public void push (T element)
   {
      if (size() == stack.length)
         expandCapacity();

      stack[top] = element;
      top++;
   }

   public T pop() throws StackEmptyException
   {
      if (isEmpty())
         throw new StackEmptyException();

      top--;
      T result = stack[top];
      stack[top] = null;

      return result;
   }

   public boolean isEmpty()
   {
	return (top == 0);
   }
}