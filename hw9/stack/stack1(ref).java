import java.util.*;
public class stack1 implements StackInterface {

    private Object[] a;
    private int count = 0;

    public stack1(int capacity){
        a = new Object[capacity];
    }

    public Object pop() throws StackEmptyException {
        if (isEmpty()) {
            throw new StackEmptyException("Stack already Empty");
        }

    public void push(Object element) throws StackEmptyException {
        if (count == a.length) {
          throw new StackEmptyException("Stack Empty.");
        }
        a[count] = element;
        count++;
    }

    public Object top() throws StackEmptyException {
        if (isEmpty()) {
            throw new StackEmptyException("Stack already Empty");
        }
        return a[count - 1];
    }

    public boolean isEmpty() {
        return topPosition==-1;
    }
        --count;
        return a[count];
    }
}