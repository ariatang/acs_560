package hw07;

public class Game {
    private int[] bowls = new int[21]; //troll times 20+1
    private int currentBowl=0;

    public void add(int pins){ // add score pin to board
        bowls[currentBowl++]=pins;
    }

    public Integer getScore(){
            int score = 0;
            int round = 0;

            for (int frame=0; frame<10; frame++){
                if(getStrike(round)){ //situation in Strike
                    score += 10 + strikeNextAllScore(round);
                    round++;
                } else if(getSpare(round)){ //situation in Spare
                    score += 10 + spareNextFirstScore(round);
                    round += 2;
                } else { //normal
                    score += currentFrameScore(round);
                    round += 2;
                }
            }
            return score;
    }

    private boolean getStrike(int round){
        return bowls[round] == 10;
    }
    private boolean getSpare(int round){
        return currentFrameScore(round) == 10;
    }
    private int currentFrameScore(int round){
        return bowls[round] + bowls[round + 1];
    }
    private int strikeNextAllScore(int round){
        return bowls[round + 1] + bowls[round + 2];
    }
    private int spareNextFirstScore(int round){
        return bowls[round + 2];
    }
}