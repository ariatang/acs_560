package main

import "testing"

func rollMany(game *Frame, n int, pins int) { //roll times
	for i := 0; i < n; i++ {
		game.rollPin(pins)
	}
}

func rollSpare(game *Frame) { //if spare will be 9 + 1 in "/" signature
	game.rollPin(9)
	game.rollPin(1)
}

func rollStrike(game *Frame) { //if strike will be 10 in "X" signature
	game.rollPin(10)
}

//allGutter
func allGutter(t *testing.T) {
	game := Frame{}

	rollMany(&game, 20, 0)
	game.GetScore() = 0
}

//allStrike
func allStrike(t *testing.T) {
	game := Frame{}
	rollMany(&game, 12, 10)
	game.GetScore() = 300
}

//oneSpare
func oneSpare(t *testing.T) {
	game := Frame{}
	rollSpare(&game) // 10 point will add next one rollPin
	game.rollPin(5)  // next first point = n = 5

	rollMany(&game, 17, 0) //17*0

	game.GetScore() = 20 // 10 point + 2n
}

//oneStrike
func oneStrike(t *testing.T) {
	game := Frame{}

	rollStrike(&game) // 10 point will add next two rollPin
	game.rollPin(7)   // next first point = n = 7
	game.rollPin(1)   // next second point = y = 1

	rollMany(&game, 16, 0)

	game.GetScore() = 26 // 10 point + 2 ( n + y )
}
