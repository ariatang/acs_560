package hw07;

import hw07.*;
import org.junit.*;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class GameTest {

	private Game game; //  => Game instance

	@Before
	public void setup() throws Exception{
		game = new Game();
	}

	@Test //allGutter
	public void allGutter() throws Exception {
		int expected = 0;
		int actual;

		this.rollMany(20, 0); //20 times
		actual = game.getScore();

		assertEquals(expected, actual);
	}


	@Test //allStrike
	public void allStrike() throws Exception{
			int expected = 300;
			int actual;

			rollMany(12,10);
			actual = game.getScore();

			assertEquals(expected, actual);
	}

	private void rollMany(int n, int pins) {
		for (int i = 0; i < n; i++)
			game.add(pins);
	}


	@Test //oneSpare
	public void oneSpare() throws Exception {
		int expected = 18; //( 8 + 2 ) + 4*2
		int actual;

		this.rollSpare();
		game.add(4); //4*2 next first throw
		this.rollMany(17,0); //17 times
		actual = game.getScore();

		assertEquals(expected, actual);
	}

	private void rollSpare() {
		game.add(8); //Spare turn
		game.add(2);
	}

	@Test //oneStrike
	public void oneStrike() throws Exception {
		int expected = 26; // 10 + 2 * ( 6 + 2 )
		int actual;

		this.rollStrike();
		game.add(6); // next first throw
		game.add(2); // next second throw
		rollMany(16, 0); //16 times
		actual = game.getScore();

		assertEquals(expected, actual);
	}

	private void rollStrike() {
		game.add(10); //Strike!!
	}

}
