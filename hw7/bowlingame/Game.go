package main

type Frame struct {
	rolls       [21]int //troll times
	currentBowl int
}

func (game *Frame) rollPin(pins int) { // add score pin to board
	game.rolls[game.currentBowl] = pins
	game.currentBowl++
}

func (game *Frame) getScore() int {
	round := 0
	score := 0
	for frame := 0; frame < 10; frame++ {
		if game.getStrike(round) {
			score += 10 + game.strikeNextAllScore(round)
			round++
		} else if game.getSpare(round) {
			score += 10 + game.spareNextFirstScore(round)
			round += 2
		} else {
			score += game.currentFrameScore(round)
			round += 2
		}
	}
	return score
}

func (game *Frame) getStrike(round int) bool {
	return (game.rolls[round] == 10)
}

func (game *Frame) getSpare(round int) bool {
	return (game.rolls[round]+game.rolls[round+1] == 10)
}

func (game *Frame) currentFrameScore(round int) int {
	return game.rolls[round] + game.rolls[round+1]
}

func (game *Frame) strikeNextAllScore(round int) int {
	return game.rolls[round+1] + game.rolls[round+2]
}

func (game *Frame) spareNextFirstScore(round int) int {
	return game.rolls[round+2]
}
