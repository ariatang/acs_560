public class Frame{
    private Bowl firstBowl;
    private Bowl secondBowl;

    public Frame (int firstScore, int secondScore){
     firstBowl = new Bowl(firstScore);
     secondBowl = new Bowl(secondScore);
    }
    public int getFrameScore(){
     return (firstBowl.getScore()+secondBowl.getScore());
    }
}