package main

import (
	"database/sql"
	"fmt"
	_ "github.com/mattn/go-sqlite3"
)

func main() {
	database, _ := sql.Open("sqlite3", "./p4.db")

	statement, _ := database.Prepare("CREATE TABLE IF NOT EXISTS Course (id INTEGER PRIMARY KEY, semester,coursename)")
	statement.Exec()
	statement, _ = database.Prepare("INSERT INTO Course (semester,coursename) VALUES (?,?)")
	statement.Exec("Fall 2018", "Software Engineering")
	statement.Exec("Spring 2018", "Software Project Management")
	rows, _ := database.Query("SELECT semester,coursename FROM Course")

	var coursename string
	var semester string
	for rows.Next() {
		rows.Scan(&semester, &coursename)
		fmt.Println(semester + "\t" + coursename)
	}
	rows.Close()
}

//  sqlite3 p4.db
//  .table
//  select* from Course;
