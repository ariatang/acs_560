package refactoring;

class ShapeTest {
    public static void main(String[] args) {
    	
    	// Square test (1)
    	double Square_length = 15;
    	Shape Square = new Square(Square_length);
    	System.out.println(
    		"Square length: " + Square_length
    	+ 	"\nSquare Area (size*size): " + Square.area());
    	
    	// Circle test (2)
    	double radius = 5;
    	Shape circle = new Circle(radius);
    	System.out.println(
    		"Circle radius: " + radius
    	+ 	"\nCircle Area (Math.PI*size*size/4.0): " + circle.area());
    		
       	// Right_Triangle test (3)
    	double Triangle_length = 10;
    	Shape Right_Triangle = new Right_Triangle(Triangle_length);
    	System.out.println(
    		"Right_Triangle side length: " + Triangle_length
    	+ 	"\nRight_Triangle Area (size * size /2.0): " + Right_Triangle.area());
   	
    	}
    }
