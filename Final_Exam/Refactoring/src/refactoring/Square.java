package refactoring;

public class Square extends Shape {
	private double size;
	
	public Square(double size) {
		super("square");
		this.size = size;
	}
	public double area() {
		return size*size;
	}
}