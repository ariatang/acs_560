package refactoring;

public class Circle extends Shape {
	private double size;
	
	public Circle(double size) {
		super("circle");
		this.size = size;
	}
	public double area() {
		return Math.PI*size*size/4.0;
	}
}