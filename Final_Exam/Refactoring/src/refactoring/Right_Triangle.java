package refactoring;

public class Right_Triangle extends Shape {
	private double size;
	
	public Right_Triangle(double size) {
		super("Right_Triangle");
		this.size = size;
	}
	public double area() {
		return size * size /2.0;
		}
}
