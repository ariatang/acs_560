package junit;

import static org.junit.Assert.*;

import org.junit.Test;

public class CalculatorTest {
	
	// Test example
	@Test
	public void testCalculator1() {
		int value = Calculator.CalculatorFromReversePolish("1 2 - 4 5 + *");
		assertEquals(-9, value);
	}
	
	// Test minus
	@Test
	public void testCalculator2() {
		int value = Calculator.CalculatorFromReversePolish("3 1 -");
		assertEquals(2, value);
	}	

	// Test plus
	@Test
	public void testCalculator3() {
		int value = Calculator.CalculatorFromReversePolish("3 1 +");
		assertEquals(4, value);
	}
	

	//Test multiply
	@Test
	public void testCalculator4() {
		int value = Calculator.CalculatorFromReversePolish("3 1 *");
		assertEquals(3, value);
	}

	// Test divide
	@Test
	public void testCalculator5() {
		int value = Calculator.CalculatorFromReversePolish("3 1 /");
		assertEquals(3, value);
	}
	
	//Test divide by 0
	@Test(expected = RuntimeException.class)
	public void testCalculator6() {
		Calculator.CalculatorFromReversePolish("0 0 /");
	}
	
	//Test no operator
	@Test(expected = RuntimeException.class)
	public void testCalculator7() {
		Calculator.CalculatorFromReversePolish("1 2 3");
	}
	
	//Test more number
	@Test(expected = RuntimeException.class)
	public void testCalculator8() {
		Calculator.CalculatorFromReversePolish("1 2 3 +");
	}
	
	//Test non number or operator
	@Test(expected = RuntimeException.class)
	public void testCalculator9() {
		Calculator.CalculatorFromReversePolish("a 1 2 3");
	}
}
