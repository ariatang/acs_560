package junit;

import static org.junit.Assert.*;

import org.junit.Test;

public class StackTest {
	@Test
	public void testStack1() {
		Stack<Integer> stack = new Stack<Integer>();
		stack.push(1);
		int value = stack.pop();
		assertEquals(1, value);
	}

	@Test
	public void testStack2() {
		Stack<Integer> stack = new Stack<Integer>();
		stack.push(1);
		stack.push(2);
		int value = stack.pop();
		assertEquals(2, value);
		value = stack.pop();
		assertEquals(1, value);
	}

	@Test
	//Pushing element on top
	public void testStack3() {
		Stack<Integer> stack = new Stack<Integer>();
		assertTrue(stack.isEmpty());
		for (int i = 0; i < 20; i++) {
			stack.push(i);
		}
	//Popping element from top
		assertFalse(stack.isEmpty());
		for (int i = 19; i >= 0; i--) {
			int value = stack.pop();
			assertEquals(i, value);
		}
		assertTrue(stack.isEmpty());
	}
}
