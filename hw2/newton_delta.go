package main

import (
	"fmt"
	"math"
)

const delta = 1e-6

func Sqrt(x float64) float64 {
	z, y := x, (0.0)
	for math.Abs(y-z) > delta {
		y, z = z, z-(z*z-x)/(2*z)
	}
	return z
}

func main() {
	a := Sqrt(2)
	b := math.Sqrt(2)

	fmt.Println(a, b, a-b)
}
