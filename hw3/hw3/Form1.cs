﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace hw3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string[] Coursename = { "Software Engineer", "Computer Architecture", "Web Application Development" };
  
       
            using (FileStream fs = new FileStream("hw3_c#_result.html", FileMode.Create)) 
            { 
                using (StreamWriter sw = new StreamWriter(fs, Encoding.UTF8)) 
                { 
                     sw.WriteLine("<html>");
                     sw.WriteLine("<style>table, th, td {border: 1px solid black;}</style>");
                     sw.WriteLine("<body>");
                     sw.WriteLine("<table>");
                     sw.WriteLine("<tr>");
                     sw.WriteLine("<th> Semester </th>");
                     sw.WriteLine("<th> Course </th>");
                     sw.WriteLine("</tr >");
                     foreach (string c in Coursename)
                    {
                        sw.WriteLine("<tr>" + "<td>Fall 2017</td>" + "<td>" + "{0}", c + "</td></tr>");
                        Console.Write("iteration", c);
                    }
                    sw.WriteLine("</table>" + "</body>" + "</html>");
                }

            } 
        } 
            
        }
}
