package main

import (
	"fmt"
	"os"
)

func main() {
	Coursename := [3]string{"Software Engineer", "Computer Architecture", "Web Application Development"}
	file, err := os.Create("hw3_go_result.html")
	if err != nil {
		fmt.Println("file creating error")
	}

	file.WriteString("<html>\n <style>table, th, td {border: 1px solid black;}</style>\n <body>\n <table>\n <tr>\n <th>Semester</th>\n <th>Course</th>\n </tr>")
	for i := 0; i < len(Coursename); i++ {
		file.WriteString("<tr>\n <td>Fall 2017</td>\n <td>" + Coursename[i] + "</td>\n </tr>")
	}
	file.WriteString("</table>\n </body>\n </html>\n")
	fmt.Println("Success")
}
