package hw10;

import java.sql.*;
public class Q2_query {
	public static void main( String args[] ) {
	      Connection c = null;
	      Statement stmt = null;
	      
	      try {	    	  
	    	  Class.forName("org.sqlite.JDBC");
	          c = DriverManager.getConnection("jdbc:sqlite:homework.db");
	          c.setAutoCommit(false);
	          System.out.println("Opened database successfully");
	          
	          // Create Statement
	          stmt = c.createStatement();
	          String sql = "CREATE TABLE COURSES (SEMESTER	TEXT, COURSE  TEXT)"; 
	          stmt.executeUpdate(sql);
	          stmt.close();
	          
	          //Insert Records
	          stmt = c.createStatement();
	          String sqlInsert = "INSERT INTO COURSES (SEMESTER, COURSE) VALUES ('Fall 2018','Software Engineering'),('Spring 2018','Software Project Management');"; 
	          stmt.executeUpdate(sqlInsert);
	          stmt.close();
	          c.commit();	          
	          
	          stmt = c.createStatement();
		      ResultSet rs = stmt.executeQuery( "SELECT * from COURSES" );
		      while ( rs.next() ) {

		    	 String SEMESTER = rs.getString("SEMESTER");
		         String COURSE  = rs.getString("COURSE");
		        
		         System.out.println( "ProfessorID = " + SEMESTER );
		         System.out.println( "Evaluation = " + COURSE );
		         System.out.println();
		      }
		      rs.close();
		      stmt.close();
		      c.close();
	       } catch ( Exception e ) {
	          System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	          System.exit(0);
	       }
	       System.out.println("Records created successfully");
	   }
}
