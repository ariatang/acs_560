package hw10;

import java.sql.*;
public class Q1_insert {
	public static void main( String args[] ) {
	      Connection c = null;
	      Statement stmt = null;
	      
	      try {
	         Class.forName("org.sqlite.JDBC");
	         c = DriverManager.getConnection("jdbc:sqlite:homework.db");
	         c.setAutoCommit(false);
	         System.out.println("Opened database successfully");

	         stmt = c.createStatement();
	         String sql = "insert into CourseEvaluation values(1, 1, 4.0);"; 
	         stmt.executeUpdate(sql);
	         
	         sql = "insert into CourseEvaluation values(2, 2, 3.5);"; 
	         stmt.executeUpdate(sql);
	          sql = "insert into CourseEvaluation values(3, 3, 4.0);"; 
	         stmt.executeUpdate(sql);
	          sql = "insert into CourseEvaluation values(4, 4, 3.3);"; 
	         stmt.executeUpdate(sql);
	          sql = "insert into CourseEvaluation values(5, 5, 3.0);"; 
	         stmt.executeUpdate(sql);
	          sql = "insert into CourseEvaluation values(6, 6, 2.0);"; 
	         stmt.executeUpdate(sql);
	          sql = "insert into CourseEvaluation values(7, 7, 2.8);"; 
	         stmt.executeUpdate(sql);
	          sql = "insert into CourseEvaluation values(8, 8, 3.5);"; 
	         stmt.executeUpdate(sql);
	          sql = "insert into CourseEvaluation values(9, 9, 3.9);"; 
	         stmt.executeUpdate(sql);
	          sql = "insert into CourseEvaluation values(10, 10, 4.0);"; 
	         stmt.executeUpdate(sql);
	          sql = "insert into CourseEvaluation values(11, 10, 3.9);"; 
	         stmt.executeUpdate(sql);
	          sql = "insert into CourseEvaluation values(12, 10, 3.8);"; 
	         stmt.executeUpdate(sql);
	          sql = "insert into CourseEvaluation values(13, 11, 4.0);"; 
	         stmt.executeUpdate(sql);
	          sql = "insert into CourseEvaluation values(14, 8, 2.1);"; 
	         stmt.executeUpdate(sql);
      

	         stmt.close();
	         c.commit();
	         c.close();
	      } catch ( Exception e ) {
	         System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	         System.exit(0);
	      }
	      System.out.println("Records created successfully");
	   }

}
