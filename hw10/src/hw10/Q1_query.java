package hw10;

import java.sql.*;
public class Q1_query {
	public static void main( String args[] ) {

		   Connection c = null;
		   Statement stmt = null;
		   try {
		      Class.forName("org.sqlite.JDBC");
		      c = DriverManager.getConnection("jdbc:sqlite:homework.db");
		      c.setAutoCommit(false);
		      System.out.println("Opened database successfully");

		      stmt = c.createStatement();
		      ResultSet rs = stmt.executeQuery( "SELECT ProfessorID, Evaluation FROM CourseEvaluation WHERE Evaluation IN (SELECT Evaluation FROM CourseEvaluation group by CourseID)group by ProfessorID ");
		      
		      while ( rs.next() ) {
		         int  ProfessorID = rs.getInt("ProfessorID");
		         int Evaluation  = rs.getInt("Evaluation");

		         System.out.println( "ProfessorID = " + ProfessorID );
		         System.out.println( "Evaluation = " + Evaluation );
		         System.out.println();
		      }
		      rs.close();
		      stmt.close();
		      c.close();
		   } catch ( Exception e ) {
		      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		      System.exit(0);
		   }
		   System.out.println("Operation done successfully");
		  }
}







