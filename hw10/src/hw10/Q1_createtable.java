package hw10;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
public class Q1_createtable {
	public static void main( String args[] ) {
	      Connection c = null;
	      Statement stmt = null;
	      
	      try {
	         Class.forName("org.sqlite.JDBC");
	         c = DriverManager.getConnection("jdbc:sqlite:homework.db");
	         System.out.println("Opened database successfully");

	         stmt = c.createStatement();
	         String sql = "create table CourseEvaluation(CourseID int primary key not null, ProfessorID int, Evaluation double);"; 
	         stmt.executeUpdate(sql);
	         stmt.close();
	         c.close();
	      } catch ( Exception e ) {
	         System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	         System.exit(0);
	      }
	      System.out.println("Table created successfully");
	   }
}
