package main

import (
	"fmt"
	"net/http"
)

type Hello struct{}

func (h Hello) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "<H1>Hello ACS560!</H2>")
}

func main() {
	var h Hello
	http.ListenAndServe("127.0.0.1:12000", h)
}
