package main

import (
	"io"
	"log"
	"net/http"
	"os"
)

func main() {
	response, err := http.Get("https://www.pfw.edu")
	if err != nil {
		log.Fatal(err)
	}
	defer response.Body.Close()

	// Create output txt file
	outFile, err := os.Create("copyCode.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer outFile.Close()

	// Copy the data from HTTP to the txt
	_, err = io.Copy(outFile, response.Body)
	if err != nil {
		log.Fatal(err)
	}
}
